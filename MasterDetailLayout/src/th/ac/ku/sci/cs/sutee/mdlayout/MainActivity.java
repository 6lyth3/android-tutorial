package th.ac.ku.sci.cs.sutee.mdlayout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements
		OnItemClickListener {

	public static final String NUMBER_FRAGMENT_TAG = "number_fragment";
	private static final String CURRENT_POSITION_TAG = "current_position";
	private int mCurrentPosition;

	private final String[] mTitles = { "One", "Two", "Three", "Four", "Five",
			"Six", "Seven", "Eight", "Nine", "Ten" };

	private NumberAdapter mAdapter;
	private ListView master;
	private boolean mDualMode = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		View detailView = findViewById(R.id.detail);
		if (detailView != null && detailView.getVisibility() == View.VISIBLE) {
			mDualMode = true;
		}

		mAdapter = new NumberAdapter(this) {
			@Override
			public Object getItem(int position) {
				return mTitles[position];
			}

			@Override
			public int getCount() {
				return mTitles.length;
			}
		};

		master = (ListView) findViewById(R.id.master);
		master.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		master.setAdapter(mAdapter);
		master.setOnItemClickListener(this);

		mCurrentPosition = savedInstanceState != null ? savedInstanceState
				.getInt(CURRENT_POSITION_TAG, 0) : 0;

		if (mDualMode) {
			changeDetail(mCurrentPosition);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(CURRENT_POSITION_TAG, mCurrentPosition);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		changeDetail(position);
	}

	private void changeDetail(int position) {
		mCurrentPosition = position;
		master.setItemChecked(position, true);
		int number = position + 1;
		if (mDualMode) {
			NumberFragment numberFragment = (NumberFragment) getSupportFragmentManager()
					.findFragmentByTag(NUMBER_FRAGMENT_TAG);
			if (numberFragment == null || numberFragment.getNumber() != number) {
				FragmentTransaction ft = getSupportFragmentManager()
						.beginTransaction();
				ft.setCustomAnimations(android.R.anim.slide_in_left,
						android.R.anim.slide_out_right);
				ft.replace(R.id.detail,
						NumberFragment.newNumberFragment(number),
						NUMBER_FRAGMENT_TAG);
				ft.commit();
			}
		} else {
			Intent intent = new Intent(this, DetailActivity.class);
			intent.putExtra(DetailActivity.NUMBER_TAG, number);
			startActivity(intent);
		}
	}
}
