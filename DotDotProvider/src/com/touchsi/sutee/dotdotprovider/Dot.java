package com.touchsi.sutee.dotdotprovider;

import android.provider.BaseColumns;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class Dot extends ModelBase {
	private Context context;
	private int id;
	private int x;
	private int y;

	public Dot() {
		super();
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void fromCursor(Cursor cursor, Context context) {
		this.id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
		this.x = cursor.getInt(cursor.getColumnIndex(DotTable.DotColumns.X));
		this.y = cursor.getInt(cursor.getColumnIndex(DotTable.DotColumns.Y));
		this.context = context;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(DotTable.DotColumns.X, this.x);
		values.put(DotTable.DotColumns.Y, this.y);
		return values;
	}

	public static Dot newInstance(Cursor cursor, Context context) {
		Dot dot = new Dot();
		dot.fromCursor(cursor, context);
		return dot;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setId(int id) {
		this.id = id;
	}

}