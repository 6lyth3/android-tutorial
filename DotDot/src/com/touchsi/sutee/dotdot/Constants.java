package com.touchsi.sutee.dotdot;

public class Constants {
	public static final String COORD_X_KEY = "COORD_X_KEY";
	public static final String COORD_Y_KEY = "COORD_Y_KEY";
	public static final String SECOND_ACTIVITY_RESULT_KEY = "SECOND_ACTIVITY_RESULT_KEY";
	
}
