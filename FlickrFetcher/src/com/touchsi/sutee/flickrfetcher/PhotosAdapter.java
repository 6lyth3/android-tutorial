package com.touchsi.sutee.flickrfetcher;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

abstract public class PhotosAdapter extends BaseAdapter implements ListAdapter {

	private static final class ViewHolder {
		ImageView imgThumb;
		TextView txtTitle;
		TextView txtOwner;
	}

	private ImageLoader mImageLoader = ImageLoader.getInstance();

	private Context mContext;
	private DisplayImageOptions mOptions;

	public PhotosAdapter(Context context) {
		mContext = context;
		mImageLoader.init(ImageLoaderConfiguration.createDefault(context));
		mOptions = new DisplayImageOptions.Builder().cacheInMemory()
				.showStubImage(R.drawable.ic_launcher).cacheOnDisc().build();
	}

	public void stopImageLoader() {
		mImageLoader.stop();
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.photo_row, parent, false);
			viewHolder.imgThumb = (ImageView) convertView
					.findViewById(R.id.imgThumb);
			viewHolder.txtOwner = (TextView) convertView
					.findViewById(R.id.txtOwner);
			viewHolder.txtTitle = (TextView) convertView
					.findViewById(R.id.txtTitle);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Photo photo = (Photo) getItem(position);
		viewHolder.txtOwner.setText(photo.getOwner());
		viewHolder.txtTitle.setText(photo.getTitle());
		mImageLoader.displayImage(photo.getThumbUrl(), viewHolder.imgThumb,
				mOptions);
		return convertView;
	}

}
